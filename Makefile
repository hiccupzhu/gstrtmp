CFLAGS=\
	-DGST_PACKAGE_NAME='"GStreamer"' \
	-DGST_PACKAGE_ORIGIN='"http://gstreamer.net"' \
	-DGST_LICENSE='"LGPL"'\
	-DPACKAGE='"rtmp"' \
	-DGETTEXT_PACKAGE='"gst-plugins-bad-1.0"'\
	-DVERSION='"0.0"' -DHAVE_USER_MTU -Wall -Wimplicit -g \
	$(shell pkg-config --cflags gstreamer-base-1.0 gstreamer-plugins-base-1.0 librtmp) -I.
	
LDFLAGS=$(shell pkg-config --libs gstreamer-base-1.0 gstreamer-plugins-base-1.0 librtmp)
INSTALL_DIR=/usr/lib64/gstreamer-1.0/
PWD=$(shell pwd)

libgstrtmp.la:gstrtmp.lo gstrtmpsink.lo gstrtmpsrc.lo
	libtool --mode=link gcc -module -shared -export-symbols-regex gst_plugin_desc $(LDFLAGS) -o $@ $+ -avoid-version -rpath $(INSTALL_DIR)

%.lo: %.c %.h
	libtool --mode=compile gcc $(CFLAGS) -fPIC -c -o $@ $<
	
.PHONY: install

install: .libs/libgstrtmp.so
	libtool --mode=install install $< $(INSTALL_DIR)
	
link:.libs/libgstrtmp.so
	ln -sf $(PWD)/$< $(INSTALL_DIR)

clean:
	rm -rf *.o *.lo *.a *.la .libs